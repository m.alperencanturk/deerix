use std::process;
use std::sync::atomic::{AtomicU32, Ordering};

use crossbeam_channel::Sender;

pub const MAX_CTRL_MAX: u32 = 3;

pub fn set_ctrl_c_handler(ctx: Sender<()>) {
    let current_ctrl_c = AtomicU32::new(MAX_CTRL_MAX);
    ctrlc::set_handler(move || {
        ctx.send(()).expect("Could not send signal on channel.");
        let new_value = current_ctrl_c.fetch_sub(1, Ordering::Acquire);
        if new_value == 0 {
            println!("<<<<<<<<<<<<<<<============ Forcefully closing app, not really recommended but if you now what you doing... ==========>>>>>>>>>>>>>>>");
            process::exit(1);
        } else {
            println!("<<<<<<<<<<<<<<<============ Trying to stop app, please wait to finish all remaining tasks, to forcefully close click {new_value} times CTRL + C. ==========>>>>>>>>>>>>>>>");
        }
    })
        .expect("Error setting Ctrl-C handler");
}
