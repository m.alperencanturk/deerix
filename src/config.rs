use std::cmp::Ordering;
use std::collections::{BTreeMap, HashMap};
use std::io::Write;
use std::path::Path;
use std::{fmt, fs, process};

use chrono::Datelike;
use config::Config as ConfigThing;
use config::File as configFile;

use crate::enums::{track_formats_to_string, TrackFormats};

#[derive(Debug)]
pub struct Config {
    pub arl: String,
    pub download_path: String,
    pub download_songs_that_failed_to_download_earlier: bool,
    pub force_download_songs: bool,
    pub user_id: i64,
    pub api_token: String,
    pub license_token: String,
    pub tracks_to_download: HashMap<u32, String>,
    pub tracks_not_downloaded: HashMap<u32, String>,
    pub albums_to_download: HashMap<u32, String>,
    pub albums_not_downloaded: HashMap<u32, String>,
    pub artists_to_download: BTreeMap<u32, (Option<Date>, String)>,
    pub ignored_songs: BTreeMap<u32, String>,
    pub concurrent_download: usize,
    pub quality: TrackFormats,
    pub fallback_quality: bool,
    pub days_waiting: u32,
    pub maximum_song_size_mb: u32,
    pub retrying_number: u32,
    pub timeout_seconds: u64,
}

impl Config {
    pub fn new() -> Self {
        Self {
            arl: String::new(),
            download_path: String::new(),
            download_songs_that_failed_to_download_earlier: true,
            force_download_songs: false,
            user_id: 0,
            api_token: String::new(),
            license_token: String::new(),
            tracks_to_download: Default::default(),
            tracks_not_downloaded: Default::default(),
            albums_to_download: Default::default(),
            albums_not_downloaded: Default::default(),
            artists_to_download: Default::default(),
            ignored_songs: Default::default(),
            concurrent_download: 0,
            quality: TrackFormats::MP3_128,
            fallback_quality: false,
            days_waiting: 10,
            maximum_song_size_mb: 0,
            retrying_number: 0,
            timeout_seconds: 0,
        }
    }
}

#[derive(Debug, Clone)]
pub struct BaseConfig {
    pub arl: String,
    pub download_path: String,
    pub download_songs_that_failed_to_download_earlier: bool,
    pub force_download_songs: bool,
    pub user_id: i64,
    pub api_token: String,
    pub license_token: String,
    pub concurrent_download: usize,
    pub quality: TrackFormats,
    pub fallback_quality: bool,
    pub days_waiting: u32,
    pub maximum_song_size_mb: u32,
    pub retrying_number: u32,
    pub timeout_seconds: u64,
}

impl BaseConfig {
    pub fn new_from_config(config: &Config) -> Self {
        Self {
            arl: config.arl.to_string(),
            download_path: config.download_path.to_string(),
            download_songs_that_failed_to_download_earlier: config.download_songs_that_failed_to_download_earlier,
            force_download_songs: config.force_download_songs,
            user_id: config.user_id,
            api_token: config.api_token.to_string(),
            license_token: config.license_token.to_string(),
            concurrent_download: config.concurrent_download,
            quality: config.quality,
            fallback_quality: config.fallback_quality,
            days_waiting: config.days_waiting,
            maximum_song_size_mb: config.maximum_song_size_mb,
            retrying_number: config.retrying_number,
            timeout_seconds: config.timeout_seconds,
        }
    }
}

#[derive(Debug, Clone)]
pub struct Date {
    pub year: u32,
    pub month: u32,
    pub day: u32,
}

impl Date {
    pub fn get_now() -> Self {
        let current_date = chrono::offset::Utc::now();
        Date {
            year: current_date.year() as u32,
            month: current_date.month(),
            day: current_date.day(),
        }
    }
    pub fn with_string(date: &str) -> Result<Self, String> {
        let split: Vec<_> = date.trim().split('-').collect();
        if split.len() != 3 {
            return Err(format!("Date {date} should contains 2 negation symbols."));
        }

        let year = match split[0].parse::<u32>() {
            Ok(t) => t,
            Err(e) => return Err(e.to_string()),
        };
        let month = match split[1].parse::<u32>() {
            Ok(t) => t,
            Err(e) => return Err(e.to_string()),
        };
        let day = match split[2].parse::<u32>() {
            Ok(t) => t,
            Err(e) => return Err(e.to_string()),
        };

        if !(1950..=2050).contains(&year) || day > 31 || month > 31 {
            return Err(format!("{date} - date out of bounds"));
        }

        Ok(Date { year, month, day })
    }
}

impl fmt::Display for Date {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}-{:02}-{:02}", self.year, self.month, self.day)
    }
}

#[test]
fn test_date() {
    assert!(Date::with_string("AAA").is_err());
    assert!(Date::with_string("2022-01-01").is_ok());

    let date1 = Date::with_string("2022-01-01").unwrap();
    let date2 = Date::with_string("2022-02-01").unwrap();
    assert!(date1 < date2);
    let date3 = Date::with_string("2022-02-01").unwrap();
    assert_eq!(date3, date2);
}

impl PartialEq<Self> for Date {
    fn eq(&self, other: &Self) -> bool {
        self.year == other.year && self.month == other.month && self.day == other.day
    }
}

impl PartialOrd<Self> for Date {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Eq for Date {}

impl Ord for Date {
    fn cmp(&self, other: &Self) -> Ordering {
        // Year
        if self.year > other.year {
            return Ordering::Greater;
        }
        if self.year < other.year {
            return Ordering::Less;
        }
        // Month
        if self.month > other.month {
            return Ordering::Greater;
        }
        if self.month < other.month {
            return Ordering::Less;
        }
        // Day
        if self.day > other.day {
            return Ordering::Greater;
        }
        if self.day < other.day {
            return Ordering::Less;
        }

        Ordering::Equal
    }
}

impl Config {
    pub fn load_config_file(&mut self) {
        let settings = match ConfigThing::builder().add_source(configFile::with_name("deerix_settings.toml")).build() {
            Ok(t) => t,
            Err(e) => {
                eprintln!("CONFIG ERROR: Failed to open config file, reason {e}");
                process::exit(1);
            }
        };

        let settings_content: HashMap<String, HashMap<String, String>> = match settings.try_deserialize::<HashMap<String, HashMap<String, String>>>() {
            Ok(t) => t,
            Err(e) => {
                eprintln!("CONFIG ERROR: Failed to parse config file, reason {e}");
                process::exit(1);
            }
        };

        {
            let _ = fs::create_dir("bak");
            let current_date = chrono::offset::Utc::now().to_string().replace(':', "_"); // Windows not supports : in name
            let backup_name = format!("bak/deerix_settings_{current_date}.toml");
            if let Err(e) = fs::copy("deerix_settings.toml", &backup_name) {
                eprintln!("CONFIG ERROR: Failed to create copy of settings in file {backup_name}, reason {e}");
                process::exit(1);
            };
        }

        // println!("{:#?}", settings_content);
        if !settings_content.contains_key("general")
            || !settings_content["general"].contains_key("arl")
            || !settings_content["general"].contains_key("download_path")
            || !settings_content["general"].contains_key("force_download_songs")
            || !settings_content["general"].contains_key("used_threads")
            || !settings_content["general"].contains_key("redownload_failed_songs")
            || !settings_content["general"].contains_key("quality")
            || !settings_content["general"].contains_key("fallback")
            || !settings_content["general"].contains_key("days_waiting")
            || !settings_content["general"].contains_key("maximum_song_size_mb")
            || !settings_content["general"].contains_key("retrying_number")
            || !settings_content["general"].contains_key("timeout_seconds")
        {
            eprintln!("CONFIG ERROR: Settings should contains general with [arl, download_path, force_download_songs, used_threads, redownload_failed_songs, quality, fallback, days_waiting, maximum_song_size_mb, retrying_number, timeout_seconds] section, look at the example to see possible values");
            process::exit(1);
        }

        let general = &settings_content["general"];
        self.arl = general["arl"].to_string();
        self.force_download_songs = general["force_download_songs"].parse::<bool>().unwrap();
        self.download_path = general["download_path"].to_string();
        self.concurrent_download = general["used_threads"].parse::<usize>().unwrap();
        self.download_songs_that_failed_to_download_earlier = general["redownload_failed_songs"].parse::<bool>().unwrap();
        self.quality = match track_formats_to_string(&general["quality"]) {
            Ok(t) => t,
            Err(e) => {
                panic!("{}", e);
            }
        };
        self.fallback_quality = general["fallback"].parse::<bool>().unwrap();
        self.days_waiting = general["days_waiting"].parse::<u32>().unwrap();
        self.maximum_song_size_mb = general["maximum_song_size_mb"].parse::<u32>().unwrap();
        self.retrying_number = general["retrying_number"].parse::<u32>().unwrap();
        self.timeout_seconds = general["timeout_seconds"].parse::<u64>().unwrap();

        if !Path::new(&self.download_path).exists() && Path::new(&self.download_path).is_dir() {
            eprintln!("CONFIG ERROR: Download path {} should exists and be a directory", self.download_path);
            process::exit(1);
        }

        let mut critical_error = false;
        for key in settings_content.keys() {
            let value = &settings_content[key];
            if key.starts_with("date_") {
                let date = match Date::with_string(key.strip_prefix("date_").unwrap()) {
                    Ok(t) => t,
                    Err(e) => {
                        eprintln!("CONFIG ERROR: Failed to parse date in {key} ,{e}");
                        critical_error = true;
                        continue;
                    }
                };
                for (artist_nr_string, comment) in value {
                    match artist_nr_string.parse::<u32>() {
                        Ok(artist_nr) => {
                            if self.artists_to_download.contains_key(&artist_nr) {
                                eprintln!("CONFIG ERROR: Found duplicated artist ID - {artist_nr_string} - only one occurrence of artist id should stay in file");
                                critical_error = true;
                                continue;
                            };
                            self.artists_to_download.insert(artist_nr, (Some(date.clone()), comment.clone()))
                        }
                        Err(e) => {
                            eprintln!("CONFIG ERROR: Failed to parse artist ID {artist_nr_string} - should be number {e}");
                            critical_error = true;
                            continue;
                        }
                    };
                }
            } else if key.starts_with("date") {
                for (artist_nr_string, comment) in value {
                    match artist_nr_string.parse::<u32>() {
                        Ok(artist_nr) => {
                            if self.artists_to_download.contains_key(&artist_nr) {
                                eprintln!("CONFIG ERROR: Found duplicated artist ID - {artist_nr_string} - only one occurrence of artist id should stay in file");
                                critical_error = true;
                                continue;
                            };
                            self.artists_to_download.insert(artist_nr, (None, comment.clone()))
                        }
                        Err(e) => {
                            eprintln!("CONFIG ERROR: Failed to parse artist ID {artist_nr_string} - should be number {e}");
                            critical_error = true;
                            continue;
                        }
                    };
                }
            } else if key.starts_with("not_downloaded_tracks") {
                for (id, comment) in value {
                    match id.parse::<u32>() {
                        Ok(id) => self.tracks_not_downloaded.insert(id, comment.clone()),
                        Err(e) => {
                            eprintln!("CONFIG ERROR: Failed to parse ID {id} - should be number {e}");
                            critical_error = true;
                            continue;
                        }
                    };
                }
            } else if key.starts_with("tracks") {
                for (id, comment) in value {
                    match id.parse::<u32>() {
                        Ok(id) => self.tracks_to_download.insert(id, comment.clone()),
                        Err(e) => {
                            eprintln!("CONFIG ERROR: Failed to parse ID {id} - should be number {e}");
                            critical_error = true;
                            continue;
                        }
                    };
                }
            } else if key.starts_with("not_downloaded_albums") {
                for (id, comment) in value {
                    match id.parse::<u32>() {
                        Ok(id) => self.albums_not_downloaded.insert(id, comment.clone()),
                        Err(e) => {
                            eprintln!("CONFIG ERROR: Failed to parse ID {id} - should be number {e}");
                            critical_error = true;
                            continue;
                        }
                    };
                }
            } else if key.starts_with("albums") {
                for (id, comment) in value {
                    match id.parse::<u32>() {
                        Ok(id) => self.albums_to_download.insert(id, comment.clone()),
                        Err(e) => {
                            eprintln!("CONFIG ERROR: Failed to parse ID {id} - should be number {e}");
                            critical_error = true;
                            continue;
                        }
                    };
                }
            } else if key.starts_with("ignored_songs") {
                for (track_id_string, comment) in value {
                    match track_id_string.parse::<u32>() {
                        Ok(t) => {
                            self.ignored_songs.insert(t, comment.clone());
                        }
                        Err(_e) => {
                            eprintln!("CONFIG ERROR: Failed get track_id value from line {track_id_string} = \"{comment}\"");
                            critical_error = true;
                            continue;
                        }
                    }
                }
            } else if key == "general" {
                continue; // Handled this is earlier
            } else {
                println!("Found unsupported settings group \"{key}\"");
                // critical_error = true; // TODO re-enable
            }
        }
        if critical_error {
            process::exit(1);
        }

        self.save_to_file();
    }

    pub fn save_to_file(&mut self) {
        let mut toml_to_save = Vec::new();
        toml_to_save.push("[general]".to_string());
        toml_to_save.push(format!("arl = \"{}\"", self.arl));
        toml_to_save.push(format!("download_path = \"{}\"", self.download_path));
        toml_to_save.push(format!(
            "force_download_songs = {} # Ignore date, and always try to download latest songs from artist",
            self.force_download_songs
        ));
        toml_to_save.push(format!(
            "used_threads = {} # 2-4 are recommended bigger values should speed up downloading, but may exceed deezer limit 50 requests per 5s",
            self.concurrent_download
        ));
        toml_to_save.push(format!("redownload_failed_songs = {}", self.download_songs_that_failed_to_download_earlier));
        toml_to_save.push(format!("quality = \"{}\"", self.quality));
        toml_to_save.push(format!("fallback = {}", self.fallback_quality));
        toml_to_save.push(format!("days_waiting = {}", self.days_waiting));
        toml_to_save.push(format!("maximum_song_size_mb = {}", self.maximum_song_size_mb));
        toml_to_save.push(format!("retrying_number = {}", self.retrying_number));
        toml_to_save.push(format!("timeout_seconds = {}", self.timeout_seconds));

        let mut none_dates: BTreeMap<u32, String> = Default::default();
        let mut artists_by_date: BTreeMap<Date, Vec<(u32, String)>> = Default::default();

        for (i, (date, comment)) in &self.artists_to_download {
            if let Some(date) = date {
                let entry = artists_by_date.entry(date.clone()).or_insert_with(Vec::new);
                entry.push((*i, comment.to_string()));
            } else {
                none_dates.insert(*i, comment.replace('\"', "'"));
            }
        }

        if !none_dates.is_empty() {
            toml_to_save.push("\n[date] # List of artists not downloaded earlier".to_string());
            for (artist_id, comment) in none_dates {
                toml_to_save.push(format!("{artist_id} = \"{comment}\""));
            }
        }
        for (date, vec_artist_comment) in artists_by_date {
            if !vec_artist_comment.is_empty() {
                toml_to_save.push(format!("\n[date_{}-{}-{}]", date.year, date.month, date.day));
                for (artist_id, comment) in vec_artist_comment {
                    toml_to_save.push(format!("{artist_id} = \"{}\"", comment.replace('\"', "'")));
                }
            }
        }

        if !self.tracks_to_download.is_empty() {
            toml_to_save.push("\n[tracks]".to_string());
            for (track_id, comment) in &self.tracks_to_download {
                toml_to_save.push(format!("{track_id} = \"{}\"", comment.replace('\"', "'")));
            }
        }
        if !self.tracks_not_downloaded.is_empty() {
            toml_to_save.push("\n[not_downloaded_tracks]".to_string());
            for (track_id, comment) in &self.tracks_not_downloaded {
                toml_to_save.push(format!("{track_id} = \"{}\"", comment.replace('\"', "'")));
            }
        }
        if !self.albums_to_download.is_empty() {
            toml_to_save.push("\n[albums]".to_string());
            for (album_id, comment) in &self.albums_to_download {
                toml_to_save.push(format!("{album_id} = \"{}\"", comment.replace('\"', "'")));
            }
        }
        if !self.albums_not_downloaded.is_empty() {
            toml_to_save.push("\n[not_downloaded_albums]".to_string());
            for (album_id, comment) in &self.albums_not_downloaded {
                toml_to_save.push(format!("{album_id} = \"{}\"", comment.replace('\"', "'")));
            }
        }
        if !self.ignored_songs.is_empty() {
            toml_to_save.push("\n[ignored_songs]".to_string());
            for (track_id, comment) in &self.ignored_songs {
                toml_to_save.push(format!("{track_id} = \"{}\"", comment.replace('\"', "'")));
            }
        }

        match fs::OpenOptions::new().write(true).create(true).truncate(true).open("deerix_settings.toml.tmp") {
            Ok(mut t) => {
                write!(t, "{}", toml_to_save.join("\n")).unwrap();
            }
            Err(e) => {
                eprintln!("CONFIG ERROR: Failed to save config file to `deerix_settings.toml.tmp`, reason {e}");
                process::exit(1);
            }
        };

        fs::copy("deerix_settings.toml.tmp", "deerix_settings.toml").unwrap();
    }
}
