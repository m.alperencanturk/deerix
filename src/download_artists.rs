use std::sync::atomic::{AtomicU32, Ordering};
use std::{process, thread};

use crossbeam_channel::{unbounded, Receiver, Sender};
use rayon::prelude::*;
use reqwest::blocking::Client;

use crate::api::{get_artist_album_tracks, get_raw_tracks_info};
use crate::config::{BaseConfig, Config, Date};
use crate::downloading::{filter_artists_by_time, generate_url_and_download_it, get_not_downloaded_songs_from_errors};
use crate::errors::DeerixError::{AlbumsGetError, ArtistNotExists, ConnectionError};
use crate::thread_messages::Messages;

pub fn downloading_artists(base_config: &BaseConfig, config: &mut Config, client: &mut Client, ctrl_c_receiver: Receiver<()>) {
    println!("Stared checking for artists to download.");
    let (tx, rx) = unbounded::<Messages>();

    let artists_to_download = filter_artists_by_time(config.artists_to_download.clone(), config.force_download_songs, config.days_waiting);
    let client_cloned = client.clone();
    let base_config = base_config.clone();
    let downloading_thread = thread::spawn(move || {
        let artist_counter = AtomicU32::new(0);
        let artists_number = artists_to_download.len() as u32;

        artists_to_download
            .into_par_iter()
            .map(|(artist_idx, (max_date, comment))| {
                let mut client = client_cloned.clone();
                let tx = tx.clone();
                let ctrl_c_receiver = ctrl_c_receiver.clone();
                download_single_artist_songs(
                    &mut client,
                    &base_config,
                    artist_counter.fetch_add(1, Ordering::Acquire),
                    artists_number,
                    artist_idx,
                    &max_date,
                    comment,
                    &tx,
                    &ctrl_c_receiver,
                );
            })
            .collect::<Vec<_>>();
    });

    let mut stats_not_downloaded_songs = 0;
    let mut stats_not_downloaded_albums = 0;
    let mut stats_ignored_songs = 0;
    let mut stats_downloaded_artists = 0;
    let mut stats_downloaded_songs = 0;
    while let Ok(value) = rx.recv() {
        // dbg!(&value);
        match value {
            Messages::DownloadedArtist(artist_id, comment, downloaded_songs, not_downloaded) => {
                // Songs
                {
                    stats_ignored_songs += not_downloaded.ignored_songs.len();
                    for (track_id, error) in &not_downloaded.ignored_songs {
                        config.ignored_songs.insert(*track_id, error.clone());
                    }

                    stats_not_downloaded_songs += not_downloaded.songs.len();
                    if !not_downloaded.songs.is_empty() {
                        config.tracks_not_downloaded.extend(not_downloaded.songs);
                    }
                }

                // Albums
                {
                    stats_not_downloaded_albums += not_downloaded.albums.len();
                    if !not_downloaded.albums.is_empty() {
                        config.albums_not_downloaded.extend(not_downloaded.albums);
                    }
                }

                stats_downloaded_artists += 1;
                stats_downloaded_songs += downloaded_songs;
                config.artists_to_download.insert(artist_id, (Some(Date::get_now()), comment.clone()));
                config.save_to_file();
                println!("Completed checking of artist {artist_id} - {comment}");
            }
            _ => {
                panic!("Message type not supported")
            }
        }
    }

    downloading_thread.join().unwrap();

    println!(
        "\nDownload completed, checked {stats_downloaded_artists} artists, downloaded {stats_downloaded_songs} songs, ignored {stats_ignored_songs} and failed to download {stats_not_downloaded_songs}, failed to download {stats_not_downloaded_albums} albums"
    );
}

fn download_single_artist_songs(
    client: &mut Client,
    base_config: &BaseConfig,
    idx: u32,
    artists_number: u32,
    artist_idx: u32,
    max_date: &Option<Date>,
    comment: String,
    tx: &Sender<Messages>,
    crx: &Receiver<()>,
) {
    let date_string = match &max_date {
        Some(t) => t.to_string(),
        None => "not downloaded yet".to_string(),
    };
    println!(
        "Started to download {} artist ({} - {}) out of all {}, last download date {:?}",
        idx + 1,
        artist_idx,
        comment,
        artists_number,
        date_string
    );
    let mut downloaded_songs = 0;
    let mut all_errors = Vec::new();

    match get_raw_tracks_info(client, base_config, artist_idx) {
        Ok((artist_struct, artist_albums)) => {
            let (artist_tracks, mut collected_errors) = get_artist_album_tracks(base_config, client, &artist_albums, &artist_struct, max_date);
            // println!("Artist - \"{}\"", artist_struct.name);
            for (track_idx, (track_id, song_data)) in artist_tracks.iter().enumerate() {
                if crx.try_recv().is_ok() {
                    println!("Closing cleanly app.");
                    process::exit(0);
                }
                println!(
                    "_____ Downloading artist track {:5}/{:<5} ({:10} - {:<20})",
                    track_idx + 1,
                    artist_tracks.len(),
                    artist_idx,
                    comment
                );

                if let Err(err) = generate_url_and_download_it(*track_id, song_data, base_config, client) {
                    all_errors.push(err);
                } else {
                    downloaded_songs += 1;
                }
            }
            all_errors.append(&mut collected_errors);
        }
        Err(e) => match e {
            // Cannot get basic info about artist, just print info without any changes(artist still will be in queue to download)
            ConnectionError(_, _, _, _) | AlbumsGetError(_, _) | ArtistNotExists(_, _) => {
                eprintln!("{e}");
                return;
            }
            e => {
                panic!("Not found error {e}");
            }
        },
    }
    // Completed downloading

    let not_downloaded = get_not_downloaded_songs_from_errors(all_errors);
    tx.send(Messages::DownloadedArtist(artist_idx, comment, downloaded_songs, not_downloaded)).unwrap();
}
