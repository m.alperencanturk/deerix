#![allow(clippy::too_many_arguments)]
#![allow(clippy::redundant_clone)]

use std::env::args;
use std::process;
use std::sync::Arc;

use crossbeam_channel::unbounded;
use reqwest::blocking::Client;
use reqwest::cookie::Jar;
use reqwest::header::HeaderMap;
use reqwest::Url;

use crate::api::search_artist_id;
use crate::config::{BaseConfig, Config};
use crate::ctrlc::set_ctrl_c_handler;
use crate::deezer::{get_cached_api_token, get_cached_license_token, get_cached_user_id, get_internal_user_data};
use crate::download_albums::downloading_albums;
use crate::download_artists::downloading_artists;
use crate::download_tracks::downloading_tracks;
use crate::enums::DeezerApiRequest;

mod api;
mod config;
mod ctrlc;
mod decoding;
mod deezer;
mod download_albums;
mod download_artists;
mod download_tracks;
mod downloading;
mod enums;
mod errors;
mod genres;
mod structs;
mod thread_messages;

fn exit_program_with_error(error_text: &str) -> ! {
    eprintln!("{error_text}");
    process::exit(1);
}

fn main() {
    let (ctx, crx) = unbounded::<()>();
    set_ctrl_c_handler(ctx);

    // Special mode to find artist_id for each artist
    if args().any(|arg| arg == "SEARCH") {
        let config = Config::new();
        let mut client = setup_client(&config.arl);
        search_artist_id(&mut client);
        process::exit(0);
    }

    let mut config = Config::new();
    config.load_config_file();

    rayon::ThreadPoolBuilder::new().num_threads(config.concurrent_download).build_global().unwrap();

    let mut client = setup_client(&config.arl);
    let user_data_json = get_internal_user_data(&mut client, DeezerApiRequest::UserData, None).unwrap();
    config.user_id = get_cached_user_id(&user_data_json);
    config.api_token = get_cached_api_token(&user_data_json);
    config.license_token = get_cached_license_token(&user_data_json);
    if config.user_id == 0 {
        exit_program_with_error("Failed to login to user account, please check you arl if is proper");
    }

    println!("Properly logged to deezer");
    println!("User id {}", config.user_id);
    // println!("Config: {:#?}", config);

    let base_config = BaseConfig::new_from_config(&config);
    if config.download_songs_that_failed_to_download_earlier {
        downloading_tracks(&base_config, &mut config, &mut client, crx.clone(), true);
        downloading_albums(&base_config, &mut config, &mut client, crx.clone(), true);
    }

    downloading_tracks(&base_config, &mut config, &mut client, crx.clone(), false);
    downloading_albums(&base_config, &mut config, &mut client, crx.clone(), false);
    downloading_artists(&base_config, &mut config, &mut client, crx.clone());
}

fn setup_client(arl: &str) -> Client {
    let mut head = HeaderMap::new();
    head.insert("Accept-Language", "en-US;q=0.7,en;q=0.3".parse().unwrap()); // Used by genre id taker
    head.insert("Connection", "keep-alive".parse().unwrap());
    let cookie = format!("arl={arl}; Domain=.deezer.com");
    let cookie_store = Jar::default();
    cookie_store.add_cookie_str(&cookie, &Url::parse("https://deezer.com").unwrap());

    Client::builder()
        .cookie_store(true)
        .cookie_provider(Arc::new(cookie_store))
        .default_headers(head)
        .build()
        .unwrap()
}
