# Deerix
Deerix is command line alternative to Deemix, which allows to automate downloading certain artists and new songs.  

Deerix contains simple settings file, that allows to manually add/edit/remove artists/albums/tracks to check. 

I created the program mainly for myself, so not all ideas will be implemented(also due to the fact that I simply lack time sometimes)

## Usage
Linux and Mac versions should work without any dependencies, Windows version require Microsoft Visual C++ libraries, that can be downloaded from official Windows site - https://aka.ms/vs/16/release/vc_redist.x64.exe (it is possible that such package is already installed on OS).

To be able to run app, first you need to create settings file with some basic info and name it `deerix_settings.toml`.  
Basic structure of toml config file is visible in [deerix_settings_example.toml](deerix_settings_example.toml) - only `[general]` section is required.

Next just run app via terminal/console (`./deerix`) and check command output to see that everything works fine(if config loads correctly and arl is valid), 
```
Properly logged to deezer
User id 11111111
```
Backup is always created when app runs, so possibility to lost configuration is really small.

To add artists to check add artist_id to `[date]` e.g.
```toml
[date]
51412122 = "91NIGHTS"
1424821 = "Lana Del Rey"
```
Id can be found in deezer artist page in url like https://www.deezer.com/en/artist/51412122 or https://www.deezer.com/en/album/134048372.  
"91NIGHTS" is comment and can contain any content(I suggest to use name of artist here)

If you already downloaded artist and not want to download same songs again, use this structure
```toml
[date_2022-04-01]
```
which tells app that you want to download music from certain artist that was released after 01 04 2022.  
To find new tracks add tracks below `[tracks]` or albums below `albums`

By default to not check over and over same artists few times a day, app skips to check artists, that were updated in last 10 days(`force_download_songs` can be set to true to bypass this).

App is resource efficient, most of the time CPU usage should less than 20%(CPU is mostly used for decoding/decrypting music files) be RAM usage should vary between 75-200MB.

## Artist finder
If you have a lot of artist names, but you don't want to search for their ID over and over again, just create `tosearch.txt` file and fill it with exact names - one in each line e.g.
```
Lykke Li
Sigala
Szemrak
```
when running `./app -- SEARCH`, `tosearchnew.txt` will be created with such content, which can be easily copied to settings file(of course after removing invalid/duplicated results) and filling missing ids when there were problems in finding of id(like in `Szemrak` in example)
```
24124 = "Lykke Li"
12441,124124,41244 = "Sigala"
= "Szemrak"
```

## Limitations/TODO
- 30/30000 downloaded music files(+30 not downloaded) in my collection are broken, so it is possible that this app not correctly decoded/downloaded them so maybe it is possible to prevent such situtation(of course it is possible that this is broken on Deezer side).
- No cli options - app can be only customized via toml file, there is no way to add artist like `./app add-artists 12024,12415,1255` - this should be done in different crate(inside this repo) and use `clap` library
- No gui - for me using config files is easier than gui with several options, but causal(or windows/mac) users are shocked that the app has no graphics interface, so probably introducing one would be useful. GUI should probably do 3 things - run app, show results in realtime(builtin text output from app) and modify settings. App probably should not touch core code, so it could be written in any language with any technology(maybe `tauri` or `slint`?)

## Contributions
Like every open source, you may:
- Create issues
- Create MR/PR with fixes and new features

## Compilation
Compilation is simple and not require any special libraries.  
Just install the newest Rust compiler(via https://rustup.rs/) and run
```bash
cargo build --release
```
produced binaries should be available in `target/release/deerix`

## Name origin 
There is an interesting, long and amazing story behind the name deerix.  
I just changed the letter `m` in deemix to `r`